FROM python:3-alpine

LABEL Autor: Daniel Requena

EXPOSE 5000

RUN mkdir /app
COPY app.py requirements.txt unit.sh int.sh /app/

WORKDIR /app
RUN pip install -r requirements.txt && chmod +x *.sh

CMD python ./app.py
