from flask import Flask, request, jsonify
app = Flask(__name__)

votes = {"meu malvado favorito": 0,
          "homem de ferro": 0,
          "a coisa": 0,
          "1984": 0,
          "querida encolhi as criancas": 0 }

@app.route("/")
def listroutes():
    links = []
    for rule in app.url_map.iter_rules():
        if rule.endpoint is not "static" and rule.endpoint is not "listroutes":
            links.append(rule.endpoint)
    return jsonify({"routes":links})

@app.route("/getmovies")
def getmovies():
    return jsonify({"filmes":list(votes.keys())})

@app.route("/vote")
def vote():
    vote = request.args.get("movie")
    if vote is not None and vote in votes.keys():
        votes[vote] = votes[vote] + 1
        return jsonify({"vote": "OK"}), 201
    else:
        return jsonify({"Error": "Not cool man"}), 404

@app.route("/getresult")
def getresult():
    return jsonify({"result": votes})

@app.route("/gettotalvotes")
def gettotalvotes():
    return jsonify({"total": sum(list(votes.values()))})

if __name__ == "__main__":
    app.run(host="0.0.0.0",debug=True)
